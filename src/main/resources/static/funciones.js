function eliminar(id){
	swal({
		  title: "¿Estas seguro de eliminar el registro?",
		  text: "Una vez eliminado el registro, no se podra recuperar",
		  icon: "warning",
		  buttons: true,
		  dangerMode: true,
		})
		.then((ok) => {
		  if (ok) {
			  $.ajax({
				 url:"/eliminar/" + id,
				 success : function(res){
					 console.log(res)
				 }
			  });
		    swal("El registro ha sido eliminado", {
		      icon: "success",
		    }).then((ok)=>{
		    	if(ok){
		    		location.href="/form"
		    	}
		    });
		  } else {
		    swal("No se elimino el registro");
		  }
		});
}

function guardadoOferta(){ 
	swal({
		title:"Guardado exitoso",
		text: "Oferta guardada correctamente",
		icon:"success",
		button: "Aceptar"
	}).then((value)=>{
		location.href="/cargaOfertaCuadros"
	});
	
}









