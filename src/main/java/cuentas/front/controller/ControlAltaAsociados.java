package cuentas.front.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.Asociados;
import cuentas.back.general.modelo.Marca;

@Controller
public class ControlAltaAsociados {

	@Value("${urlBackend}")
	private String urlBackend;

	private RestTemplate rest = null;
	
	@GetMapping("/asociados")
	public String iniciarDatosPantalla(Model model)
	{ 
		Asociados asociado = new Asociados();
		model.addAttribute("asociado", asociado);
		
		List<Asociados> lstAsociados = recuperaLstAdosciados();
		model.addAttribute("lstAsociados", lstAsociados);
		
		List<Marca> lstMarca = recuperaLstMarcas();
		model.addAttribute("marcas", lstMarca);
		
		return "frmAsociados";
	}

	@PostMapping("/asociados")
	public String guarda(Asociados socio, Model model) {
		complementaAsociado(socio);
		rest = new RestTemplate();
		rest.postForEntity(urlBackend + "/socio/save", socio, Asociados.class);
		
		System.out.println("Retorna el response Entity");
		return "redirect:/asociados";
	}
	
	
	@GetMapping("/socio/editar/{asociadoId}")
	public String actualiza(@PathVariable int asociadoId, Model model) { 
		
		rest = new RestTemplate();
		Asociados asociado = rest.getForObject(urlBackend + "/socio/update/{asociadoId}", Asociados.class, asociadoId);
		model.addAttribute("asociado", asociado);
		
		List<Asociados> lstAsociados = recuperaLstAdosciados();
		model.addAttribute("lstAsociados", lstAsociados);
		
		List<Marca> lstMarca = recuperaLstMarcas();
		model.addAttribute("marcas", lstMarca);
		
		return "frmAsociados";
	}
	
	@GetMapping("/socio/eliminar/{asociadoId}")
	public String elimina(@PathVariable int asociadoId, Model model) {
		Map<String, Integer> params = new  HashMap<String, Integer>();
		params.put("asociadoId", asociadoId);
		
		rest = new RestTemplate();
		rest.delete(urlBackend + "/socio/eliminar/{asociadoId}", params);
		
		System.out.println();
		
		return "redirect:/asociados";
	}
	
	private void complementaAsociado(Asociados socio) {
		socio.setEstatus(new Integer(socio.getDescEstatus().toString()));
		socio.setFechoract(new Date());
		socio.setUsuarioId(2);
	}
	
	private List<Asociados> recuperaLstAdosciados() {
		rest = new RestTemplate();
		
		ResponseEntity<List<Asociados>> objAsociados = rest.exchange(urlBackend + "/socio/consulta", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Asociados>>() { });
		List<Asociados> lstAsociados = objAsociados.getBody();
		return lstAsociados;
	}
	
	private List<Marca> recuperaLstMarcas() {
		rest = new RestTemplate();
		ResponseEntity<List<Marca>> objMarca = rest.exchange(urlBackend + "/marca/listarMarca", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<Marca>>() { });
		List<Marca> lstMarca = objMarca.getBody();
		return lstMarca;
	}
	
}
