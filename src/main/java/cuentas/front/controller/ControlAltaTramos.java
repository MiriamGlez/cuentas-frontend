package cuentas.front.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.TramoMarca;
import cuentas.back.general.modelo.Marca;
import cuentas.back.general.modelo.Via;

@Controller
public class ControlAltaTramos {
	@Value("${urlBackend}")
	private String urlBackend;

	private RestTemplate rest = null;

	@GetMapping("/tramos")
	public String listarMarcaRol(Model model) {
		
		TramoMarca tramo = new TramoMarca();
		model.addAttribute("tramoMarca",tramo);
		
		List<Marca> lstMarca = recuperaLstMarcas();
		model.addAttribute("marcas", lstMarca);
		
		List<Via> lstVias = recuperaLstVias();
		model.addAttribute("vias", lstVias);
		
		List<TramoMarca> lstTramosMarca = recuperaTramosMarca();
		model.addAttribute("lstTramosMarca", lstTramosMarca);

		return "frmtramos";
	}
	
	
	@PostMapping("/tramos")
	public String guarda(TramoMarca tramo, Model model) {
		complementaTramo(tramo);
		rest = new RestTemplate();
		rest.postForEntity(urlBackend + "/tramo/guarda", tramo, TramoMarca.class);
		
		System.out.println();
		return "redirect:/tramos";
	}
	
	@GetMapping("/tramos/editar/{idTramoMarca}")
	public String actualizar(@PathVariable int idTramoMarca, Model model) { 
		
		rest = new RestTemplate();
		TramoMarca tramo = rest.getForObject(urlBackend + "/tramo/update/{idTramoMarca}", TramoMarca.class, idTramoMarca);
		model.addAttribute("tramoMarca",tramo);
		
		List<Marca> lstMarca = recuperaLstMarcas();
		model.addAttribute("marcas", lstMarca);
		
		List<Via> lstVias = recuperaLstVias();
		model.addAttribute("vias", lstVias);
		
		List<TramoMarca> lstTramosMarca = recuperaTramosMarca();
		model.addAttribute("lstTramosMarca", lstTramosMarca);
		
		return "frmtramos";
	}
	
	
	@GetMapping("/tramos/eliminar/{idTramoMarca}")
	public String eliminar(@PathVariable int idTramoMarca,  Model model) {
		
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("idTramoMarca", idTramoMarca);
		rest = new RestTemplate();
		
		rest.delete(urlBackend + "/tramosMarca/delete/{idTramoMarca}", params);
		
		System.out.println();
		return "redirect:/tramos";
	}
	
	private void complementaTramo(TramoMarca tramo) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR, 01);
		cal.set(Calendar.MINUTE, 00);
		
		tramo.setEstatus(1);
		tramo.setFecHorAct(new Date());
		tramo.settRecorrido(cal.getTime());
		tramo.setUsuarioId(1);
		if(tramo.isSelecPista()) { 
			tramo.setConPista(1);
		}else { 
			tramo.setConPista(0);
		}
	}


	private List<Marca> recuperaLstMarcas() {
		rest = new RestTemplate();
		ResponseEntity<List<Marca>> objMarca = rest.exchange(urlBackend + "/marca/listarMarca", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Marca>>() {
				});
		List<Marca> lstMarca = objMarca.getBody();
		return lstMarca;
	}
	
	private List<Via> recuperaLstVias(){ 
		rest = new RestTemplate();
		ResponseEntity<List<Via>> objVia = rest.exchange(urlBackend + "/via/consultaVia", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Via>>() {
				});
		List<Via> lstVias = objVia.getBody();
		return lstVias;
	}
	
	private List<TramoMarca> recuperaTramosMarca(){ 
		rest = new RestTemplate();
		ResponseEntity<List<TramoMarca>> objTramos = rest.exchange(urlBackend + "/tramoMarca/consulta", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TramoMarca>>() {
				});
		List<TramoMarca> lstTramosMarca = objTramos.getBody();
		return lstTramosMarca;
	}
}
