package cuentas.front.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.DetalleMarca;
import cuentas.back.ccuentas.modelo.DetalleMarcaIds;
import cuentas.back.ccuentas.modelo.DetalleMarcaKey;
import cuentas.back.ccuentas.modelo.MarcaRol;
import cuentas.back.general.modelo.Marca;

@Controller
public class ControlAltaDetalleMarca {

	@Value("${urlBackend}")
	private String urlBackend;
	
	private RestTemplate rest = null;
	
	@GetMapping("/detalleMarca/{idMarca}")
	public String inicioPantalla(@PathVariable Integer idMarca, Model model) {
		
		DetalleMarca detalle = new DetalleMarca();
		model.addAttribute("detMarca", detalle);
		
		return "dlg/DlgDetalle";
	}
	
	@PostMapping("/detalleMarca")
	public String save(@RequestBody DetalleMarcaIds detalleM, Model model) {
		DetalleMarca detalle = armaObjDetalleMarca(detalleM);
		
		rest = new RestTemplate();
		rest.postForEntity(urlBackend+ "/detalleMarca/guarda", detalle, DetalleMarca.class);
		
		return "redirect:frmMarcas";
	}

	private DetalleMarca armaObjDetalleMarca(DetalleMarcaIds detalleM) {
		
		DetalleMarca detalle = new DetalleMarca();
		DetalleMarcaKey key = new DetalleMarcaKey();
		Marca marca = new Marca();
		marca.setId(detalleM.getIdMarca());
		MarcaRol marcaRol = new MarcaRol();
		marcaRol.setRolId(detalleM.getIdMarcaRol());
		
		key.setMarca(marca);
		key.setMarcaRol(marcaRol);
		detalle.setKey(key);
		
		detalle.setCuentasPermitidas(detalleM.getCuentasPermitidas());
		detalle.setEstatus(1);
		detalle.setFactorKms(detalleM.getFactorKms());
		detalle.setFechaFianza(new Date());
		detalle.setFecHorAct(new Date());
		detalle.setUsuarioId(1);
		detalle.setValorCuenta(detalleM.getValorCuenta());
		detalle.setValorFianza(detalleM.getValorFianza());
		
		return detalle;
	}
}
