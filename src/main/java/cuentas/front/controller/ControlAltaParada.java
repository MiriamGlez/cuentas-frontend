package cuentas.front.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.Parada;

@Controller
public class ControlAltaParada {
	
	@Value("${urlBackend}")
	private String urlBackend;
	
	private RestTemplate rest = null;
	
	@GetMapping("/parada")
	public String iniciaPantalla(Model model) {
		
		Parada parada = new Parada();
		model.addAttribute("parada", parada);
		
		List<Parada> lstParadas = recuperaParadas();
		model.addAttribute("lstParadas", lstParadas);
		
		return "frmParadas";
	}
	
	private List<Parada> recuperaParadas() {
		rest = new RestTemplate();
		
		ResponseEntity<List<Parada>> objParada = rest.exchange(urlBackend + "/parada/lista", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Parada>>() { });
		List<Parada> lstParadas = objParada.getBody();
		return lstParadas;
	}

	@PostMapping("/parada")
	public String guarda(Parada p, Model model) {
		complementaParada(p);
		rest = new RestTemplate();
		rest.postForEntity(urlBackend + "/parada/guarda", p, Parada.class);
		
		System.out.println("");
		
		return "redirect:/parada";
	}

	private void complementaParada(Parada p) {
		p.setFecHorAct(new Date());
		p.setUsuarioId(2);
		if(p.isValorEstatus()) { 
			p.setStatus(1);
		}else {
			p.setStatus(0);
		}
	}
	
	@GetMapping("/parada/editar/{idParada}")
	public String actualizar(@PathVariable int idParada, Model model) {
		rest = new RestTemplate();
		
		Parada parada =  rest.getForObject(urlBackend + "/parada/update/{idParada}", Parada.class, idParada);
		model.addAttribute("parada", parada);
		
		List<Parada> lstParadas = recuperaParadas();
		model.addAttribute("lstParadas", lstParadas);
		return "frmParadas";
	}
	
	@GetMapping("/parada/eliminar/{idParada}")
	public String eliminar(@PathVariable int idParada, Model model) {
		
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("idParada", idParada);
		
		rest = new RestTemplate();
		rest.delete(urlBackend + "/parada/delete/{idParada}", params);
		
		return "redirect:/parada";
	}
	

	@RequestMapping(value = "/parada/clave", method = RequestMethod.GET)
	@ResponseBody
	public List<Parada> recuperaParada(@RequestParam(value = "clave", required = true) String clave,@RequestParam(value = "desc", required = true) String desc,  Model model) {
		rest = new RestTemplate();
		ResponseEntity<List<Parada>> objParada = rest.exchange(urlBackend + "/parada/busqueda/{variable},{variable2}", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<Parada>>() { }, clave, desc);
		List<Parada> lstParadas = objParada.getBody();
		
		model.addAttribute("lstParadas", lstParadas);
		
		return lstParadas;
	}
}
