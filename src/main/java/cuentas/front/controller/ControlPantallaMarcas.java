package cuentas.front.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.MarcaRol;
import cuentas.back.general.modelo.Marca;
import cuentas.back.general.modelo.Usuario;

@Controller
@RequestMapping
public class ControlPantallaMarcas {

	@Value("${urlBackend}")
	private String urlBackend;

	private RestTemplate rest = null;
	
	@GetMapping("/form")
	public String listarMarcaRol(Model model) {

		MarcaRol marcarol = new MarcaRol();
		model.addAttribute("marcaRol", marcarol);

		List<Marca> lstMarca = recuperaLstMarcas();
		model.addAttribute("marcas", lstMarca);
		
		List<MarcaRol> lstMarcaRol = recuperaLstMarcaRol();
		model.addAttribute("lstMarcaRol", lstMarcaRol);
		
		return "frmMarcas";
	}
	
	@PostMapping("/form")
	public String save(MarcaRol marcaRol, Model model) {
		complementaMarcaRol(marcaRol);
		
		rest =  new RestTemplate();
		rest.postForEntity(urlBackend + "/marcaRol/save", marcaRol, MarcaRol.class);
		
		System.out.println("Retorna el Response Entity");
		return "redirect:/form";
	}
	
	@GetMapping("/eliminar/{rolId}")
	public String delete(@PathVariable int rolId, Model model) {
		
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("rolId", rolId);
	     
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete ( urlBackend+"/eliminar/{rolId}",  params );
		
		System.out.println();
		return "redirect:/form";
	}
	
	@GetMapping("/editar/{rolId}")
	public String update(@PathVariable int rolId, Model model) {
		rest = new RestTemplate();
	    MarcaRol marcarol = rest.getForObject(urlBackend + "/update/{rolId}", MarcaRol.class, rolId);
	    model.addAttribute("marcaRol", marcarol);

		List<Marca> lstMarca = recuperaLstMarcas();
		model.addAttribute("marcas", lstMarca);
		
		List<MarcaRol> lstMarcaRol = recuperaLstMarcaRol();
		model.addAttribute("lstMarcaRol", lstMarcaRol);
		
		return "frmMarcas";
	}

	private List<Marca> recuperaLstMarcas() {
		rest = new RestTemplate();
		ResponseEntity<List<Marca>> objMarca = rest.exchange(urlBackend + "/marca/listarMarca", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<Marca>>() { });
		List<Marca> lstMarca = objMarca.getBody();
		return lstMarca;
	}
	
	private List<MarcaRol> recuperaLstMarcaRol() {
		rest = new RestTemplate();
		ResponseEntity<List<MarcaRol>> objMarcaRol = rest.exchange(urlBackend + "/marcaRol/listarMarcaRol", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<MarcaRol>>() { });
		List<MarcaRol> lstMarcaRol = objMarcaRol.getBody();
		return lstMarcaRol;
	}

	private void complementaMarcaRol(MarcaRol marcaRol) {
		Usuario usuario = creaUsuario();
		marcaRol.setEstatus(new Integer(1));
		marcaRol.setFecHorAct(new Date());
		marcaRol.setUsuario(usuario);		
	}
	private Usuario creaUsuario() {
		Usuario usuario = new Usuario();
		usuario.setIdUsuario(new Integer(3));
		return usuario;
	}
}
