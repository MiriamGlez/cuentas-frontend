package cuentas.front.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.RolOferta;

@Controller
public class ControlGeneraDetalleRol {

	@Value("${urlBackend}")
	private String urlBackend;

	private RestTemplate rest = null;
	
	@GetMapping("oferta/detalle")
	public List<?> generaDetalleOferta(RolOferta rol, Model model) { 
		rol = generaRol();
		rest = new RestTemplate();
		
		List<?> lst = (List<?>) rest.postForEntity(urlBackend + "/oferta/armaDetalle", rol, RolOferta.class);
		
		return lst;
	}
	
	private RolOferta generaRol() { 
		Calendar cFrecuencia = Calendar.getInstance();
		cFrecuencia.setTime(new Date());
		cFrecuencia.set(Calendar.MINUTE, 20);
		cFrecuencia.set(Calendar.SECOND, 0);
		
		Calendar cInicio = Calendar.getInstance();
		cInicio.set(Calendar.HOUR, 05-12);
		cInicio.set(Calendar.MINUTE, 00);
		cInicio.set(Calendar.SECOND, 0);
		
		Calendar cTermino = Calendar.getInstance();
		cTermino.set(Calendar.HOUR, 07-12);
		cTermino.set(Calendar.MINUTE, 30);
		cTermino.set(Calendar.SECOND, 0);
		
		Calendar cTrecorrido = Calendar.getInstance();
		cTrecorrido.set(Calendar.HOUR, 1-12);
		cTrecorrido.set(Calendar.MINUTE, 30);
		cTrecorrido.set(Calendar.SECOND, 0);
		cTrecorrido.set(Calendar.MILLISECOND, 0);
		
		RolOferta rol = new RolOferta();
		rol.setCveDestino("PCH");
		rol.setCveOrigen("MXD");
		rol.setDescRol("ROL VOCHO");
		rol.setDescVia("FEDERAL");
		rol.setFrecuencia(new Date(cFrecuencia.getTime().getTime()));
		rol.setHoraInicio(new Date(cInicio.getTime().getTime()));
		rol.setHoraTermina(new Date(cTermino.getTime().getTime()));
		rol.settRecorrido(new Date(cTrecorrido.getTime().getTime()));
		rol.setIdDestino(1);
		rol.setIdOrigen(2);
		rol.setIdRol(300);
		rol.setIdVia(14);
		rol.setKms(30.00);
		return rol;
	}
	
}
