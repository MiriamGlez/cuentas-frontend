package cuentas.front.controller;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.AutobusExterno;
import cuentas.back.ccuentas.modelo.GeneracionCuadros;
import cuentas.back.ccuentas.modelo.MarcaRol;
import cuentas.back.ccuentas.modelo.Parada;
import cuentas.back.ccuentas.modelo.ParametrosValorOferta;
import cuentas.back.ccuentas.modelo.TramoMarca;
import cuentas.back.ccuentas.modelo.ValorOferta;
import cuentas.back.ccuentas.modelo.Viajes;
import cuentas.back.general.modelo.Autobus;
import cuentas.back.general.modelo.Empleado;
import cuentas.back.general.modelo.Marca;
import cuentas.back.general.modelo.Via;

@Controller
public class ValidadorOferta {
	
	private Boolean guardado = null;
	private String mensajeExcel = null;

	private MarcaRol marcaRol = null;
	
	private Vector<ValorOferta> vDatos = null;
	private LinkedHashMap<Object, Object> hshTramosMarca = null;
	private LinkedHashMap<Object, Object> hshTramoMarcaMtto = null;
	private LinkedHashMap<Object, Object> hshVias = null;
	private LinkedHashMap<Object, Object> hshConductores = null;
	private LinkedHashMap<Object, Object> hshAutobuses = null;
	private LinkedHashMap<Object, Object> hshAutobusesExt = null;
	private LinkedHashMap<Object, Object> hshParadas = null;
	
	private Vector<Object> vRolMarcasTramo = null;
	
	private String urlBackend = "http://localhost:8090/";

	private RestTemplate rest = null;
	
	public boolean validaEmpateOferta(LinkedHashMap<Object, Object> hshCveOrigen, LinkedHashMap<Object, Object> hshCveDestino, ParametrosValorOferta parametros) {
		Vector<ValorOferta> oferta = parametros.getValorOferta();
		Iterator<?> it = oferta.iterator();
		while (it.hasNext()) {
			ValorOferta valor = (ValorOferta) it.next();
			String cveOrigen = valor.getOrigen();
			String cveDestino = valor.getDestino();
			if (hshCveOrigen.containsKey(cveOrigen)) {
				Vector<Object> descripcion = (Vector<Object>) hshCveOrigen.get(cveOrigen);
				descripcion.add(cveOrigen);
			} else {
				Vector<Object> descripcion = new Vector<>();
				descripcion.add(cveOrigen);
				hshCveOrigen.put(cveOrigen, descripcion);
			}

			if (hshCveDestino.containsKey(cveDestino)) {
				Vector<Object> descripcion = (Vector<Object>) hshCveDestino.get(cveDestino);
				descripcion.add(cveDestino);
			} else {
				Vector<Object> descripcion = new Vector<>();
				descripcion.add(cveDestino);
				hshCveDestino.put(cveDestino, descripcion);
			}
		}
		return contabilizaOferta(hshCveOrigen, hshCveDestino);
	}

	private boolean contabilizaOferta(LinkedHashMap<Object, Object> hshCveOrigen, LinkedHashMap<Object, Object> hshCveDestino) {
		int origen = 0;
		int destino = 0;
		Iterator<?> itOrigen = hshCveOrigen.keySet().iterator();
		while (itOrigen.hasNext()) {
			String cveOrigen = (String) itOrigen.next();
			Vector<Object> lstCve = (Vector<Object>) hshCveOrigen.get(cveOrigen);
			System.out.println(lstCve);
			origen++;
		}

		Iterator<?> itDestino = hshCveDestino.keySet().iterator();
		while (itDestino.hasNext()) {
			String cveDestino = (String) itDestino.next();
			Vector<Object> lstCve = (Vector<Object>) hshCveDestino.get(cveDestino);
			System.out.println(lstCve);
			destino++;
		}
		if (origen != destino) {
			return false;
		}
		return true;
	}

//	public void validaCampoPorCampo(String descripcionMarca, String descripcionMarcarol, LinkedHashMap<Object, Object> hshVias, ParametrosValorOferta parametros) {
////		setGuardado(true);
//		Marca marca = recuperaMarca(descripcionMarca);
//		if(marca == null) {
//			setMensajeExcel("La marca que intenta cargar en el archivo, no existe.");
//			return; 
//		}
//		cargaTramosMarca(marca);
//		cargaMarcaRol(descripcionMarcarol);
//		llenaVias(hshVias);
//
//		SimpleDateFormat formato = new SimpleDateFormat("HH:mm");
//		setvDatos(new Vector<>());
//
//		Vector<ValorOferta> valores = parametros.getValorOferta();
//		Iterator<?> it = valores.iterator();
//		while (it.hasNext()) {
//			Vector<Object> renglon = new Vector<>();
//
//			ValorOferta valor = (ValorOferta) it.next();
//			String descripcion = validaDiasVSValor(valor);
//			validaCampos(valor);
//			
//			String origen = valor.getOrigen();
//			String destino = valor.getDestino();
//			
//			Via via = (Via) getHshVias().get(valor.getVia());
////			
//			TramoMarca tramoMarca = (TramoMarca) getHshTramosMarca().get(origen + "-" + destino+"-"+via.getId());
//			if(tramoMarca == null){ 
//				setGuardado(false);
//				descripcion += "No cuenta con kms ni vias";
//			}
//			
//			//HORA DE SALIDA 
//			valor.setDescripcion(descripcion);
//			renglon.add(descripcion);
//			renglon.add(valor.getCuadro());
//			renglon.add(valor.getSecuencia());
//			renglon.add(valor.getHoraSalida() == null ? "" : formato.format(valor.getHoraSalida()));
//			
//			renglon.add(valor.getOrigen());
//			renglon.add(valor.getDestino());
//			
//
//			renglon.add(tramoMarca == null ? "" : tramoMarca.getKmsRecorridos().toString());
//			renglon.add(tramoMarca == null ? "" : tramoMarca.getVia().getNombVia());
//
//			renglon.add(valor.getAplicaL());
//			renglon.add(valor.getAplicaM());
//			renglon.add(valor.getAplicaMI());
//			renglon.add(valor.getAplicaJ());
//			renglon.add(valor.getAplicaV());
//			renglon.add(valor.getAplicaS());
//			renglon.add(valor.getAplicaD());
//			renglon.add(valor.getNumDias());
//			renglon.add(valor.getValorL());
//			renglon.add(valor.getValorM());
//			renglon.add(valor.getValorMI());
//			renglon.add(valor.getValorJ());
//			renglon.add(valor.getValorV());
//			renglon.add(valor.getValorS());
//			renglon.add(valor.getValorD());
//
//			getvDatos().add(valor);
////			getvDatos().add(renglon);
//		}
//	}
	
	public void validaCampoPorCampo(String descripcionMarca, String descripcionMarcarol, LinkedHashMap<Object, Object> hshVias, ParametrosValorOferta parametros, GeneracionCuadros generacion) {
		Vector<GeneracionCuadros> vGeneracion = new Vector<GeneracionCuadros>();
		Marca marca = recuperaMarca(descripcionMarca);
		if(marca == null) {
			setMensajeExcel("La marca que intenta cargar en el archivo, no existe.");
			return; 
		}
		cargaTramosMarca(marca);
		cargaMarcaRol(descripcionMarcarol);
		llenaVias(hshVias);

		SimpleDateFormat formato = new SimpleDateFormat("HH:mm");
		setvDatos(new Vector<>());

		Vector<ValorOferta> valores = parametros.getValorOferta();
		Iterator<?> it = valores.iterator();
		while (it.hasNext()) {

			ValorOferta valor = (ValorOferta) it.next();
			String descripcion = validaDiasVSValor(valor);
			validaCampos(valor);
			
			String origen = valor.getOrigen();
			String destino = valor.getDestino();
			
			Via via = (Via) getHshVias().get(valor.getVia());
//			
			TramoMarca tramoMarca = (TramoMarca) getHshTramosMarca().get(origen + "-" + destino+"-"+via.getId());
			if(tramoMarca == null){ 
				setGuardado(false);
				descripcion += "No cuenta con kms ni vias";
			}
			
			//HORA DE SALIDA 
			valor.setDescripcion(descripcion);
			valor.setKms(tramoMarca == null ? "" : tramoMarca.getKmsRecorridos().toString());
			valor.setVia(tramoMarca == null ? "" : tramoMarca.getVia().getNombVia());
			
			valor.setDescMarca(marca.getDescMarca());
			valor.setDescMarcaRol(getMarcaRol().getDescripcion());
			valor.setIdMarca(marca.getId());
			valor.setIdMarcaRol(getMarcaRol().getRolId());
			
			generacion.setIdMarca(marca.getId());
			generacion.setMarca(marca.getDescMarca());
			generacion.setIdMarcaRol(getMarcaRol().getRolId());
			generacion.setMarcaRol(getMarcaRol().getDescripcion());
			vGeneracion.add(generacion);
			valor.setGeneraCuadros(vGeneracion);
			getvDatos().add(valor);
		}
	}
	
	private Marca recuperaMarca(String descripcion) {
		rest = new RestTemplate();
		ResponseEntity<Marca> objMarca = rest.exchange(urlBackend + "/marca/porDesc/{descripcion}", HttpMethod.GET, null,
				new ParameterizedTypeReference<Marca>() { }, descripcion);
		Marca marca = objMarca.getBody();
		System.out.println();
		
		return marca;
	}
	
	private void cargaTramosMarca(Marca marca) {
		rest = new RestTemplate();
		ResponseEntity<List<TramoMarca>> objMarca = rest.exchange(urlBackend + "/tramoMarca/porMarcaId/{id}", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TramoMarca>>() { }, marca.getId());
		List<TramoMarca> lstTramosMarca = objMarca.getBody();
		armaDatos(lstTramosMarca);
		System.out.println();
	}
	
	private void armaDatos(List<TramoMarca> lstRolMarcaTramo) 
	{
		setHshTramosMarca(new LinkedHashMap<>());
		setHshTramoMarcaMtto(new LinkedHashMap<>());
		try {
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			setvRolMarcasTramo(new Vector<>());
			if( lstRolMarcaTramo == null || lstRolMarcaTramo.isEmpty() ) return;
			
			Iterator<?> it = lstRolMarcaTramo.iterator();
			while(it.hasNext()){
				Vector<Object> renglon = new Vector<>();
				TramoMarca rol = (TramoMarca) it.next();
				
				String origen = rol.getOrigen().getCveParada();
				String destino = rol.getDestino().getCveParada();
				
				renglon.add(rol);
				renglon.add(rol.getMarca().getDescMarca());
				renglon.add(origen);
				renglon.add(destino);
				renglon.add(rol.getKmsRecorridos());
				renglon.add(rol.getCosto());
				renglon.add(rol.getVia().getNombVia());
				String llave = rol.getMarca().getId()+"-"+rol.getOrigen().getParadaId()+"-"+rol.getDestino().getParadaId()+"-"+rol.getVia().getId();

				getvRolMarcasTramo().add(renglon);
				
				getHshTramosMarca().put(origen+"-"+destino+"-"+rol.getVia().getId(), rol);
				
				getHshTramoMarcaMtto().put(llave, rol);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void cargaMarcaRol(String descripcionMarcarol) {
		rest = new RestTemplate();
		ResponseEntity<MarcaRol> objMarca = rest.exchange(urlBackend + "/marcaRol/porDescripcion/{descripcionMarcarol}", HttpMethod.GET, null,
				new ParameterizedTypeReference<MarcaRol>() { }, descripcionMarcarol);
		setMarcaRol(objMarca.getBody());
		System.out.println();
	}
	
	private void llenaVias(LinkedHashMap<Object, Object> hshDescVia){
		setHshVias(new LinkedHashMap<>());
		rest = new RestTemplate();
		ResponseEntity<List<Via>> objVia = rest.exchange(urlBackend + "/via/consultaVia", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Via>>() {
				});
		List<Via> lstVias = objVia.getBody();
		armaHash(lstVias);
		System.out.println();
	}
	
	private void armaHash(List<Via> lista) {
		Iterator<?> it = lista.iterator();
		while (it.hasNext()) {
			Object obj = (Object) it.next();
			if (obj instanceof Empleado) {
				Empleado empleado = (Empleado) obj;
				getHshConductores().put(empleado.getCveEmpleado(), empleado);
			}
			if (obj instanceof Autobus) {
				Autobus bus = (Autobus) obj;
				getHshAutobuses().put(bus.getNumeroEconomico(), bus);
			}
			if (obj instanceof AutobusExterno) {
				AutobusExterno bus = (AutobusExterno) obj;
				getHshAutobusesExt().put(bus.getNumeroEconomico(), bus);
			}
			if (obj instanceof Parada) {
				Parada parada = (Parada) obj;
				getHshParadas().put(parada.getCveParada(), parada);
			}
			if(obj instanceof Via){
				Via via = (Via)obj;
				getHshVias().put(via.getNombVia(), via);
			}
		}
	}
	
	private String obtenDescripciones(LinkedHashMap<Object, Object> lista) {
		String descripcion = "";
		String comilla = "'";
		Iterator<?> it = lista.keySet().iterator();
		while (it.hasNext()) {

			String llave = (String) it.next();
			descripcion += comilla + llave + comilla;
			if (it.hasNext()) {
				descripcion += ",";
			}
		}
		return descripcion;
	}
	
	private String validaDiasVSValor(ValorOferta valor) {
		String descripcion = "";
		descripcion += validaCamposAplicaValor(valor.getAplicaL(), valor.getValorL(), "Aplica L", "Valor L");
		descripcion += validaCamposAplicaValor(valor.getAplicaM(), valor.getValorM(), "Aplica M", "Valor M");
		descripcion += validaCamposAplicaValor(valor.getAplicaMI(), valor.getValorMI(), "Aplica MI", "Valor MI");
		descripcion += validaCamposAplicaValor(valor.getAplicaJ(), valor.getValorJ(), "Aplica J", "Valor J");
		descripcion += validaCamposAplicaValor(valor.getAplicaV(), valor.getValorV(), "Aplica V", "Valor V");
		descripcion += validaCamposAplicaValor(valor.getAplicaS(), valor.getValorS(), "Aplica S", "Valor S");
		descripcion += validaCamposAplicaValor(valor.getAplicaD(), valor.getValorD(), "Aplica D", "Valor D");
		return descripcion;
	}
	
	private String validaCamposAplicaValor(String aplicaD, String valorD, String descAD, String descVD) {
		String desc = "";
		if (!aplicaD.equals("")) {
			if (valorD.equals("")) {
				desc = "El campo " + descAD + " no tiene valor para el campo : " + descVD;
				setGuardado(false);
			}
		}
		return desc;
	}
	
	private void validaCampos(ValorOferta valor) {
		if (valor.getOrigen() == null || valor.getDestino() == null || valor.getKms() == null) {
			setGuardado(false);
		} 
	}
	
	public Boolean getGuardado() {
		return guardado;
	}

	public void setGuardado(Boolean guardado) {
		this.guardado = guardado;
	}

	public String getMensajeExcel() {
		return mensajeExcel;
	}

	public void setMensajeExcel(String mensajeExcel) {
		this.mensajeExcel = mensajeExcel;
	}

	public Vector<ValorOferta> getvDatos() {
		return vDatos;
	}

	public void setvDatos(Vector<ValorOferta> vDatos) {
		this.vDatos = vDatos;
	}

	public LinkedHashMap<Object, Object> getHshTramosMarca() {
		return hshTramosMarca;
	}

	public void setHshTramosMarca(LinkedHashMap<Object, Object> hshTramosMarca) {
		this.hshTramosMarca = hshTramosMarca;
	}

	public MarcaRol getMarcaRol() {
		return marcaRol;
	}

	public void setMarcaRol(MarcaRol marcaRol) {
		this.marcaRol = marcaRol;
	}

	public LinkedHashMap<Object, Object> getHshVias() {
		return hshVias;
	}

	public void setHshVias(LinkedHashMap<Object, Object> hshVias) {
		this.hshVias = hshVias;
	}

	public Vector<Object> getvRolMarcasTramo() {
		return vRolMarcasTramo;
	}

	public void setvRolMarcasTramo(Vector<Object> vRolMarcasTramo) {
		this.vRolMarcasTramo = vRolMarcasTramo;
	}

	public LinkedHashMap<Object, Object> getHshTramoMarcaMtto() {
		return hshTramoMarcaMtto;
	}

	public void setHshTramoMarcaMtto(LinkedHashMap<Object, Object> hshTramoMarcaMtto) {
		this.hshTramoMarcaMtto = hshTramoMarcaMtto;
	}

	public LinkedHashMap<Object, Object> getHshConductores() {
		return hshConductores;
	}

	public void setHshConductores(LinkedHashMap<Object, Object> hshConductores) {
		this.hshConductores = hshConductores;
	}

	public LinkedHashMap<Object, Object> getHshAutobuses() {
		return hshAutobuses;
	}

	public void setHshAutobuses(LinkedHashMap<Object, Object> hshAutobuses) {
		this.hshAutobuses = hshAutobuses;
	}

	public LinkedHashMap<Object, Object> getHshAutobusesExt() {
		return hshAutobusesExt;
	}

	public void setHshAutobusesExt(LinkedHashMap<Object, Object> hshAutobusesExt) {
		this.hshAutobusesExt = hshAutobusesExt;
	}

	public LinkedHashMap<Object, Object> getHshParadas() {
		return hshParadas;
	}

	public void setHshParadas(LinkedHashMap<Object, Object> hshParadas) {
		this.hshParadas = hshParadas;
	}
	
	
	
	
	
}
