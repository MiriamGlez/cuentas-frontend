package cuentas.front.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.Asociados;
import cuentas.back.ccuentas.modelo.AutobusExterno;
import cuentas.back.general.modelo.Marca;
import cuentas.back.general.modelo.Region;
import cuentas.back.general.modelo.Zona;

@Controller
public class ControlAltaAutobusExterno {

	@Value("${urlBackend}")
	private String urlBackend;

	private RestTemplate rest = null;
	
	@GetMapping("/autobusExterno")
	public String iniciaPantalla(Model model) {
		
		AutobusExterno bus = new AutobusExterno();
		model.addAttribute("autobusExt", bus);
		
		List<Region> lstRegiones = recuperaLstRegion();
		model.addAttribute("regiones", lstRegiones);
		
		List<Asociados> lstAsociados = recuperaLstAdosciados();
		model.addAttribute("asociados", lstAsociados);
		
		List<AutobusExterno> lstBusExterno = recuperaLstAutbosExt();
		model.addAttribute("lstBusExterno", lstBusExterno);
		
		return "frmAutobusExterno";
	}
	
	@PostMapping("/autobusExterno")
	public String guarda(AutobusExterno bus, Model model) {
		complementaAutobus(bus);
		rest = new RestTemplate();
		rest.postForEntity(urlBackend + "/bus/save", bus, AutobusExterno.class);
		
		System.out.println("");
		
		return "redirect:/autobusExterno";
	}
	
	private void complementaAutobus(AutobusExterno bus) {
		bus.setEstatus(recuperaEstatus(bus.isValorEstatus()));
		bus.setFecHorAct(new Date());
		bus.setIdUsuario(2);
		bus.setIdTipoAutobus(2);
	}

	private Integer recuperaEstatus(boolean valorEstatus) {
		if(valorEstatus) { 
			return 1;
		}
		return 0;
	}

	@GetMapping("/bus/editar/{idAutobus}")
	public String actualizar(@PathVariable int idAutobus, Model model) { 
		rest = new RestTemplate();
		
		AutobusExterno bus = rest.getForObject(urlBackend + "/bus/update/{idAutobus}", AutobusExterno.class, idAutobus);
		model.addAttribute("autobusExt", bus);
		
		List<Region> lstRegiones = recuperaLstRegion();
		model.addAttribute("regiones", lstRegiones);
		
		List<Asociados> lstAsociados = recuperaLstAdosciados();
		model.addAttribute("asociados", lstAsociados);
		
		List<AutobusExterno> lstBusExterno = recuperaLstAutbosExt();
		model.addAttribute("lstBusExterno", lstBusExterno);
		
		return "frmAutobusExterno";
	}
	
	@GetMapping("/bus/eliminar/{idAutobus}")
	public String eliminar(@PathVariable int idAutobus, Model model) {
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("idAutobus", idAutobus);
		
		rest = new RestTemplate();
		rest.delete(urlBackend + "/busExt/eliminar/{idAutobus}", params);
		System.out.println();
		
		return "redirect:/autobusExterno";
	}
	
	// SE REALIZAN LAS CONSULTAS INICIALES DE LA PANTALLA
	private List<AutobusExterno> recuperaLstAutbosExt() {
		rest = new RestTemplate();
		
		ResponseEntity<List<AutobusExterno>> objBusEx = rest.exchange(urlBackend + "/busExt/consulta", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AutobusExterno>>() { });
		List<AutobusExterno> lstAsociados = objBusEx.getBody();
		return lstAsociados;
	}

	private List<Asociados> recuperaLstAdosciados() {
		rest = new RestTemplate();
		
		ResponseEntity<List<Asociados>> objAsociados = rest.exchange(urlBackend + "/socio/consulta", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Asociados>>() { });
		List<Asociados> lstAsociados = objAsociados.getBody();
		return lstAsociados;
	}
	
	// CODIGO DEL LLENADO DE LOS COMBOS 
	
	private List<Region> recuperaLstRegion() {
		rest = new RestTemplate();
		ResponseEntity<List<Region>> objRegion = rest.exchange(urlBackend + "/region/consulta", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Region>>() { });
		List<Region> lstAsociados = objRegion.getBody();
		return lstAsociados;
	}
	
	
	@RequestMapping(value = "/marcas_region", method = RequestMethod.GET)
	@ResponseBody
	public List<Marca> recuperaMarcas(@RequestParam(value = "tipo", required = true) String tipo,  Model model){
		rest = new RestTemplate();
		ResponseEntity<List<Marca>> objMarca = rest.exchange(urlBackend + "/marca/porRegio/{id}", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Marca>>() { }, tipo);
		List<Marca> lstMarcas = objMarca.getBody();
		System.out.println();
		
		return lstMarcas;
	}
	
	@RequestMapping(value="/zona_marca", method = RequestMethod.GET)
	@ResponseBody
	public List<Zona> recuperaZonasMarca(@RequestParam(value = "tipo", required = true) String tipo)
	{ 
		rest = new RestTemplate();
		ResponseEntity<List<Zona>> objZona = rest.exchange(urlBackend + "/zona/porMarcas/{id}", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Zona>>() {}, tipo);
		List<Zona> lstZonas = objZona.getBody();
		
		return lstZonas;
	}
}
