package cuentas.front.controller.operativo;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.MarcaRol;
import cuentas.back.ccuentas.modelo.ParamBuscaViajes;
import cuentas.back.ccuentas.modelo.Viajes;
import cuentas.back.general.modelo.Marca;

@Controller
public class ControlConsultaViajes {

	@Value("${urlBackend}")
	private String urlBackend;
	
	@Value("${urlBackendOperativo}")
	private String urlBackendOperativo;
	
	private ParamBuscaViajes params = null;
	
	private RestTemplate rest = null;
	
	@GetMapping("/viajes")
	public String iniciaDatos(ParamBuscaViajes parametros, Model model) {
		setParams(parametros);
		System.out.println();
		
		model.addAttribute("params", getParams());
		
		List<MarcaRol> lstMarcaRol = recuperaLstMarcaRol();
		model.addAttribute("lstMarcaRol", lstMarcaRol);
		
		List<Marca> lstMarca = recuperaLstMarcas();
		model.addAttribute("marcas", lstMarca);
		
		if(getParams().getMarcaId() != null) {
			List<Viajes> lstViajes = recuperaLstViajes(getParams().getMarcaId(), getParams().getMarcaRolId(), getParams().getFecha());
			model.addAttribute("lstViajes",lstViajes);
		}
		return "/operativo/FrmConsultaViajes";
	}
	
	private List<Viajes> recuperaLstViajes(Integer marcaId, Integer marcarolId, String fecha) {
		rest = new RestTemplate();
		ResponseEntity<List<Viajes>> objViajes = rest.exchange(urlBackendOperativo + "/consulta/viajes/{marcaId},{marcaRolId},{fecha}", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<Viajes>>() { }, marcaId, marcarolId, fecha);
		List<Viajes> lstViajes = objViajes.getBody();
		return lstViajes;
	}

	private List<Marca> recuperaLstMarcas() {
		rest = new RestTemplate();
		ResponseEntity<List<Marca>> objMarca = rest.exchange(urlBackend + "/marca/listarMarca", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<Marca>>() { });
		List<Marca> lstMarca = objMarca.getBody();
		return lstMarca;
	}
	
	private List<MarcaRol> recuperaLstMarcaRol() {
		rest = new RestTemplate();
		ResponseEntity<List<MarcaRol>> objMarcaRol = rest.exchange(urlBackend + "/marcaRol/listarMarcaRol", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<MarcaRol>>() { });
		List<MarcaRol> lstMarcaRol = objMarcaRol.getBody();
		return lstMarcaRol;
	}
	
	
	@RequestMapping(value = "/viajes/confirmar", method = RequestMethod.POST)
	@ResponseBody
	public String viajesConfirmados(@RequestParam(value = "viajeId", required = true) Integer viajeId, Model modal) {
		
		rest = new RestTemplate();

		ResponseEntity<String> confirmar = rest.exchange(urlBackendOperativo + "/viajes/confirmar/{viajeId},{marcaId},{marcaRolId},{fecha}", HttpMethod.GET, null, 
				new ParameterizedTypeReference<String>() { }, viajeId, getParams().getMarcaId(), getParams().getMarcaRolId(), getParams().getFecha());
		String mensaje = confirmar.getBody();
		if(mensaje == null) {
			mensaje = "Guardado Exitoso";
		}
		setParams(getParams());
		return mensaje;
	}

	public ParamBuscaViajes getParams() {
		return params;
	}

	public void setParams(ParamBuscaViajes params) {
		this.params = params;
	}
}
