package cuentas.front.controller;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cuentas.back.ccuentas.modelo.ValorOferta;

@Controller
public class ControlCargaOferta {
	
	private Vector<ValorOferta> ofertaViajes = null;
	private String marca = null;
	private String marcaRol = null;
	
	@Value("${urlBackend}")
	private String urlBackend;

	private RestTemplate rest = null;
	
	@GetMapping("/cargaOfertaCuadros")
	public String cargaDatosIniciales(Model model)
	{
		model.addAttribute("lstOferta", null);
		return "frmCargaOferta";
	}

	@PostMapping("/cargaOfertaCuadros")
	public String cargaArchivo(@RequestParam("file") MultipartFile file, RedirectAttributes attributes, Model model) throws ParseException {

		ControlLecturaArchivo control = new ControlLecturaArchivo();
		setOfertaViajes(new Vector<>());
		if (file.isEmpty()) {
			attributes.addFlashAttribute("message", "Favor de seleccionar un archivo");
			return "redirect:/cargaOfertaCuadros";
		}
		 File convFile = new File(file.getOriginalFilename());
		 try {
			convFile.createNewFile();
			setOfertaViajes(control.lecturaArchivo(convFile));
			setMarca(control.getDescripcionMarca());
			setMarcaRol(control.getDescripcionMarcarol());
			
			model.addAttribute("lstOferta", getOfertaViajes());
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return "frmCargaOferta";
	}
	
	
//	@RequestMapping(value = "/enviaOferta/fechas", method = RequestMethod.POST)
//	@ResponseBody
//	public void enviaOferta(@RequestParam(value = "fecIni", required = true) String fecIni,@RequestParam(value = "fecFin", required = true) String fecFin,  Model model) throws ParseException {
//		System.out.println();
//		ControlGuardaOferta control = new ControlGuardaOferta();
//
//		Date fecInicial=new SimpleDateFormat("yyyy-MM-dd").parse(fecIni);
//		Date fecFinal =new SimpleDateFormat("yyyy-MM-dd").parse(fecFin);
//		
//		control.guardaOferta(fecInicial, fecFinal, getMarca(), getMarcaRol(), getOfertaViajes());
//		System.out.println();
//	}
	
	@RequestMapping(value = "/enviaOferta/fechas", method = RequestMethod.POST)
	@ResponseBody
	public String guardaOferta(@RequestParam(value = "fecIni", required = true) String fecIni,@RequestParam(value = "fecFin", required = true) String fecFin,  Model model) throws ParseException {
		rest = new RestTemplate();
		
//		rest.exchange(urlBackend + "/oferta/save", getOfertaViajes(), ResponseEntity.class);
		
//		JSONObject json = new JSONObject();
//		json.put("fecIni", fecIni);
//		json.put("lista", getOfertaViajes());
//		
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		
//		HttpEntity<JSONObject> httpEntity = new HttpEntity<JSONObject>(json, headers);
//		
//		rest.exchange(urlBackend + "/oferta/save", HttpMethod.GET, httpEntity, String.class);
		
		System.out.println();
//		ResponseEntity<String> confirmar = rest.exchange(urlBackend + "/oferta/save", HttpMethod.GET, null, 
//				new ParameterizedTypeReference<String>() { }, json);
//		String mensaje = confirmar.getBody();
		return "";
	}
	
	public Vector<ValorOferta> getOfertaViajes() {
		return ofertaViajes;
	}

	public void setOfertaViajes(Vector<ValorOferta> ofertaViajes) {
		this.ofertaViajes = ofertaViajes;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getMarcaRol() {
		return marcaRol;
	}

	public void setMarcaRol(String marcaRol) {
		this.marcaRol = marcaRol;
	}
}
