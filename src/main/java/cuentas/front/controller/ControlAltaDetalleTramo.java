package cuentas.front.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.DetalleTramo;

@Controller
public class ControlAltaDetalleTramo {

	@Value("${urlBackend}")
	private String urlBackend;
	
	private RestTemplate rest = null;
	
	
	@RequestMapping(value = "/detalleT/idTramo", method = RequestMethod.GET)
	@ResponseBody
	public List<DetalleTramo> recuperaParada(@RequestParam(value = "idTramo", required = true) Integer idTramo,  Model model) {
		rest = new RestTemplate();
		ResponseEntity<List<DetalleTramo>> objParada = rest.exchange(urlBackend + "/detalleTramo/busqueda/{idTramo}", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<DetalleTramo>>() { }, idTramo);
		List<DetalleTramo> lstDetTramo = objParada.getBody();
		
		model.addAttribute("lstDetTramo", lstDetTramo);
		
		return lstDetTramo;
	}
}
