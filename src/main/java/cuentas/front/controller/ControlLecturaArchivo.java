package cuentas.front.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import cuentas.back.ccuentas.constantes.Constantes;
import cuentas.back.ccuentas.modelo.GeneracionCuadros;
import cuentas.back.ccuentas.modelo.ParametrosValorOferta;
import cuentas.back.ccuentas.modelo.ValorOferta;
import cuentas.back.ccuentas.modelo.Viajes;

public class ControlLecturaArchivo {
	
	private String mensajeExcel = null;
	private String descripcionMarca = "";
	private String descripcionMarcarol = "";
	
	private ValidadorOferta validador = new ValidadorOferta();
	
	private ParametrosValorOferta params = new ParametrosValorOferta();

	private LinkedHashMap<Object, Object> hshParadas = null;
	private LinkedHashMap<Object, Object> hshMarca = null;
	private LinkedHashMap<Object, Object> hshMarcaRol = null;
	private LinkedHashMap<Object, Object> hshVias = null;

	private GeneracionCuadros generacion = null;
	public Vector<ValorOferta> lecturaArchivo(File ruta) throws ParseException {
		setMensajeExcel(null);
		SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ROOT);

		setHshParadas(new LinkedHashMap<>());
		setHshMarca(new LinkedHashMap<>());
		setHshMarcaRol(new LinkedHashMap<>());
		setHshVias(new LinkedHashMap<>());
		setParams(new ParametrosValorOferta());
		setGeneracion(new GeneracionCuadros());
		InputStream excel = null;

		try {
			excel = new FileInputStream(ruta);

			HSSFWorkbook workbook = new HSSFWorkbook(excel);
			Sheet hssfSheet = workbook.getSheetAt(0);
			Row hssfRow;
			int rows = hssfSheet.getLastRowNum();
			int cols = 0;
			String valorCelda = "";

			Vector<ValorOferta> vDatos = new Vector<>();

			for (int renglon = 0; renglon < rows + 1; renglon++) {
				hssfRow = hssfSheet.getRow(renglon);
				if (hssfRow == null) {
					setMensajeExcel("El archivo a cargar, no es el correcto");
					return null;
				} else {
					System.out.print("Row: " + renglon + " -> ");
					if (renglon == 0) {
						for (int c = 0; c < (cols = hssfRow.getLastCellNum()); c++) {
							
							if(c == 0){
								valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
								if(!valorCelda.equals("Marca")){
									setMensajeExcel("El archivo a cargar, no es el correcto");
									return null;
								}
							}
							
							if (c == 1) {
								valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
								getParams().setMarca(valorCelda);
								setDescripcionMarca(valorCelda);
								getHshMarca().put(valorCelda, "");
							}
						}
					}
					if (renglon == 1) {
						for (int c = 0; c < (cols = hssfRow.getLastCellNum()); c++) {
							if (c == 1) {
								valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
								getParams().setTramoMarca(valorCelda);
								setDescripcionMarcarol(valorCelda);
								getHshMarcaRol().put(valorCelda, "");
							}
						}
					}
					if (renglon == 2) {
						for (int c = 0; c < (cols = hssfRow.getLastCellNum()); c++) {
							if (c == 1) {
								valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
								getParams().setNumCuadros(quitaDecimal(valorCelda));
								getGeneracion().setCuadros(new Integer(quitaDecimal(valorCelda)));
							}
						}
					}
					if (renglon == 3) {
						for (int c = 0; c < (cols = hssfRow.getLastCellNum()); c++) {
							if (c == 1) {
								valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
								getParams().setNumVueltas(quitaDecimal(valorCelda));
								getGeneracion().setVueltas(new Integer(quitaDecimal(valorCelda)));
							}
						}
					}
					if (renglon == 4) {
						for (int c = 0; c < (cols = hssfRow.getLastCellNum()); c++) {
							if (c == 1) {
								valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
								getParams().setDescTipoOferta(valorCelda);
								getGeneracion().setTipoOferta(valorCelda);
							}
						}
					}
					if (renglon >= 6) {

						ValorOferta valor = new ValorOferta();
						for (int c = 0; c < (cols = hssfRow.getLastCellNum()); c++) {
							valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
							if (c == Constantes.CELDA_CUADRO) {
								valor.setCuadro(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_SECUENCIA) {
								valor.setSecuencia(valorCelda);
							}
							if (c == Constantes.CELDA_HORASALIDA) {
								valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
								if (!valorCelda.equals("") && !valorCelda.equals(" ")) {
									Date currentdate = sdf.parse(valorCelda);
									valor.setHoraSalida(currentdate);
								}
							}
							if (c == Constantes.CELDA_ORIGEN) {
								if (!valorCelda.equals("")) {
									valor.setOrigen(valorCelda.toUpperCase());
									getHshParadas().put(valorCelda.toUpperCase(), "");
								}
							}
							if (c == Constantes.CELDA_DESTINO) {
								if (!valorCelda.equals("")) {
									valor.setDestino(valorCelda.toUpperCase());
									getHshParadas().put(valorCelda.toUpperCase(), "");
								}
							}
							if (c == Constantes.CELDA_KMS) {
								valor.setKms(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_VIA) {
								valorCelda = recuperaValorCelda(valorCelda, hssfRow, c);
								valor.setVia(valorCelda.toUpperCase());
								getHshVias().put(valorCelda.toUpperCase(), "");
							}
							if (c == Constantes.CELDA_AL) {
								valor.setAplicaL(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_AM) {
								valor.setAplicaM(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_AW) {
								valor.setAplicaMI(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_AJ) {
								valor.setAplicaJ(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_AV) {
								valor.setAplicaV(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_AS) {
								valor.setAplicaS(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_AD) {
								valor.setAplicaD(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_NUMDIAS) {
								valor.setNumDias(quitaDecimal(valorCelda));
							}
							if (c == Constantes.CELDA_VL) {
								valor.setValorL(valorCelda);
							}
							if (c == Constantes.CELDA_VM) {
								valor.setValorM(valorCelda);
							}
							if (c == Constantes.CELDA_VW) {
								valor.setValorMI(valorCelda);
							}
							if (c == Constantes.CELDA_VJ) {
								valor.setValorJ(valorCelda);
							}
							if (c == Constantes.CELDA_VV) {
								valor.setValorV(valorCelda);
							}
							if (c == Constantes.CELDA_VS) {
								valor.setValorS(valorCelda);
							}
							if (c == Constantes.CELDA_VD) {
								valor.setValorD(valorCelda);
							}
						}
						vDatos.add(valor);
						getParams().setValorOferta(vDatos);
					}
				}
			}
			System.out.println();
			validaDatos();
			return validador.getvDatos();
		} catch (FileNotFoundException fileNotFoundException) {
			System.out.println("No se encontró el archivo : " + fileNotFoundException);
		} catch (IOException ex) {
			System.out.println("Error al procesar el archivo :  " + ex);
		} finally {
			try {
				excel.close();
			} catch (IOException ex) {
				System.out.println(" Error al procesar el archivo después de cerrarlo : " + ex);
			}
		}
		return null;
	}
	
	private String recuperaValorCelda(String valorCelda, Row hssfRow, int c) {

		if (hssfRow.getCell(c) == null) {
			return "";
		} else if (hssfRow.getCell(c).getCellType() == CellType.STRING) {
			return "" + hssfRow.getCell(c).getStringCellValue();
		} else if (hssfRow.getCell(c).getCellType() == CellType.BOOLEAN) {
			return "" + hssfRow.getCell(c).getBooleanCellValue();
		} else if (hssfRow.getCell(c).getCellType() == CellType.NUMERIC) {
			if (HSSFDateUtil.isCellDateFormatted(hssfRow.getCell(c))) {
				valorCelda = "" + hssfRow.getCell(c).getDateCellValue();
				return valorCelda;
			} else {
				return "" + hssfRow.getCell(c).getNumericCellValue();
			}
		} else if (hssfRow.getCell(c).getCellType() == CellType.FORMULA) {
			return "" + hssfRow.getCell(c).getNumericCellValue();
		} else if (hssfRow.getCell(c).getCellType() == CellType.BLANK) {
			return "";
		}
		return "";
	}
	
	private String quitaDecimal(String valorCelda) {
		String[] val = valorCelda.split(Pattern.quote("."));
		return val[0];
	}
	
	private boolean validaDatos() {
		boolean valida = validador.validaEmpateOferta(new LinkedHashMap<>(), new LinkedHashMap<>(), getParams());
		if (!valida) {
			return false;
		}
		validador.validaCampoPorCampo(getDescripcionMarca(), getDescripcionMarcarol(), getHshVias(), getParams(), getGeneracion());
		return true;
	}
	
	public String getMensajeExcel() {
		return mensajeExcel;
	}

	public void setMensajeExcel(String mensajeExcel) {
		this.mensajeExcel = mensajeExcel;
	}
	
	public ParametrosValorOferta getParams() {
		return params;
	}

	public void setParams(ParametrosValorOferta params) {
		this.params = params;
	}

	public LinkedHashMap<Object, Object> getHshParadas() {
		return hshParadas;
	}

	public void setHshParadas(LinkedHashMap<Object, Object> hshParadas) {
		this.hshParadas = hshParadas;
	}

	public LinkedHashMap<Object, Object> getHshMarca() {
		return hshMarca;
	}

	public void setHshMarca(LinkedHashMap<Object, Object> hshMarca) {
		this.hshMarca = hshMarca;
	}

	public LinkedHashMap<Object, Object> getHshMarcaRol() {
		return hshMarcaRol;
	}

	public void setHshMarcaRol(LinkedHashMap<Object, Object> hshMarcaRol) {
		this.hshMarcaRol = hshMarcaRol;
	}

	public LinkedHashMap<Object, Object> getHshVias() {
		return hshVias;
	}

	public void setHshVias(LinkedHashMap<Object, Object> hshVias) {
		this.hshVias = hshVias;
	}
	
	public String getDescripcionMarcarol() {
		return descripcionMarcarol;
	}

	public void setDescripcionMarcarol(String descripcionMarcarol) {
		this.descripcionMarcarol = descripcionMarcarol;
	}

	public String getDescripcionMarca() {
		return descripcionMarca;
	}

	public void setDescripcionMarca(String descripcionMarca) {
		this.descripcionMarca = descripcionMarca;
	}

	public GeneracionCuadros getGeneracion() {
		return generacion;
	}

	public void setGeneracion(GeneracionCuadros generacion) {
		this.generacion = generacion;
	}
}
