package cuentas.front.controller;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.modelo.GeneracionCuadros;
import cuentas.back.ccuentas.modelo.MarcaRol;
import cuentas.front.controller.xls.GeneraXlsCuadros;

@Controller
public class ControlGeneracionOferta {

	@Value("${urlBackend}")
	private String urlBackend;

	private RestTemplate rest = null;
	
	@GetMapping("/ofertaCuadros")
	public String cargaDatosIniciales(Model model)
	{
		GeneracionCuadros oferta = new GeneracionCuadros();
		model.addAttribute("ofertaC", oferta);
		
		List<MarcaRol> lstMarcaRol = recuperaLstMarcaRol();
		model.addAttribute("lstMarcaRol", lstMarcaRol);
		
		return "FrmGeneracionOferta";
	}
	
	@PostMapping("/ofertaCuadros")
	public ResponseEntity<InputStreamResource> recuperaFormulario(GeneracionCuadros oferta, Model model) {

		System.out.println("");
		GeneraXlsCuadros cuadros = new GeneraXlsCuadros();
		ByteArrayInputStream stream = cuadros.generaExcelCuadros(oferta);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=ofertaCuadros.xls");
		System.out.println();
		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
	
	private List<MarcaRol> recuperaLstMarcaRol() {
		rest = new RestTemplate();
		ResponseEntity<List<MarcaRol>> objMarcaRol = rest.exchange(urlBackend + "/marcaRol/listarMarcaRol", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<MarcaRol>>() { });
		List<MarcaRol> lstMarcaRol = objMarcaRol.getBody();
		return lstMarcaRol;
	}
	
}
