package cuentas.front.controller;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import cuentas.back.ccuentas.constantes.Constantes;
import cuentas.back.ccuentas.modelo.DetalleRol;
import cuentas.back.ccuentas.modelo.DetalleTramo;
import cuentas.back.ccuentas.modelo.MarcaRol;
import cuentas.back.ccuentas.modelo.Rol;
import cuentas.back.ccuentas.modelo.TramoMarca;
import cuentas.back.ccuentas.modelo.ValorOferta;
import cuentas.back.ccuentas.modelo.Viajes;
import cuentas.back.general.modelo.Marca;

@Controller
public class ControlGuardaOferta {

	private String urlBackend = "http://localhost:8090/";

	private RestTemplate rest = null;
	
	private Vector<Object> detallesRol = new Vector<>();
	private Rol rol = null;
	
	private Vector<Object> ofertaViajes = null;
	private Vector<Object> lstViaje = null;
	
	private String descMarca = null;
	private String descMarcaRol = null;
	
	private Vector<Object> lstDetalleTramo = null;
	private LinkedHashMap<Object, Object> hshDetalles = null;
	private LinkedHashMap<Object, Object> hshTramosMarca = null;
	
	public String guardaOferta(Date fecIni, Date fecFin, String descMarca, String descMarcaRol, Vector<Object> ofertaViajes) {
		setOfertaViajes(ofertaViajes);
		setDescMarca(descMarca);
		setDescMarcaRol(descMarcaRol);
		
		Marca marca = recuperaMarca(descMarca);
		MarcaRol marcaRol = recuperaMarcaRol(descMarcaRol);
		
		Rol rol = recuperaRol(marca.getId(), marcaRol.getRolId(), fecIni);
		
		armaModeloDatos(fecIni, fecFin, null); //fecha Inicial, fecha final y rol
		return "guardado";
	}
	
	private Marca recuperaMarca(String descripcion) {
		rest = new RestTemplate();
		ResponseEntity<Marca> objMarca = rest.exchange(urlBackend + "/marca/porDesc/{descripcion}", HttpMethod.GET, null,
				new ParameterizedTypeReference<Marca>() { }, descripcion);
		Marca marca = objMarca.getBody();
		System.out.println();
		
		return marca;
	}
	
	private MarcaRol recuperaMarcaRol(String descripcion) { 
		rest = new RestTemplate();
		ResponseEntity<MarcaRol> objMarcaRol = rest.exchange(urlBackend + "/marcaRol/porDescripcion/{descripcionMarcarol}", HttpMethod.GET, null,
				new ParameterizedTypeReference<MarcaRol>() { }, descripcion);
		MarcaRol marcaRol = objMarcaRol.getBody();
		return marcaRol;
	}
	
	public Rol recuperaRol(Integer marcaId, Integer marcaRolId, Date fecha) {
		rest = new RestTemplate();
		ResponseEntity<Rol> objRol = rest.exchange(urlBackend + "/marca/porDesc/{marcaId}, {marcaRolId},{fecha}", HttpMethod.GET, null,
				new ParameterizedTypeReference<Rol>() { }, marcaId, marcaRolId, fecha);
		Rol rol = objRol.getBody();
		System.out.println();
		return rol;
	}

	private void armaModeloDatos(Date fecInicial, Date fechaFinal, Rol rol) {
		creaModeloRol(fecInicial,fechaFinal);
		creaDetalleRol();
		creaViajes(fecInicial, fechaFinal);
	}

	private void creaModeloRol(Date fecInicial, Date fechaFinal) {
		
		Marca marca = recuperaMarca(getDescMarca());
		MarcaRol marcaRol = recuperaMarcaRol(getDescMarcaRol());
		
		Rol rol = new Rol();
		rol.setFecHorAct(new Date());
		rol.setMarcaId(marca.getId());
		rol.setMarcaRolId(marcaRol.getRolId());
		rol.setNumCuadros(new Integer(4));
		rol.setNumVueltas(new Integer(4));
		rol.setPeriodoInicial(fecInicial);
		rol.setPeriodoFinal(fechaFinal);
		rol.setTipoOferta(1);//recuperaTipoOferta("vacacional"));
		rol.setUsuarioId(1);
//		Rol rol = guardaRol(rol);
//		rol.setRolId(rolId);
		setRol(rol);
	}

	private void creaViajes(Date fecInicial, Date fechaFinal) {
		cargaDetallesTramo();
		
		setLstViaje(new Vector<>());
		int numDias = obtieneDiferenciaDias(fechaFinal, fecInicial) + 1;

		Iterator<?> it = getDetallesRol().iterator();
		while (it.hasNext()) {

			DetalleRol detalles = (DetalleRol) it.next();
			armaSecuenciaViajes(detalles, fecInicial, fechaFinal, numDias);
		}

		pintaviajesconsola();
		
	}
	
	private void cargaDetallesTramo() {
		List<DetalleTramo> lstTramosMarca = recuperaTramosMarca();
		armaDetalles(lstTramosMarca);
	}
	
	private List<DetalleTramo> recuperaTramosMarca(){ 
		rest = new RestTemplate();
		ResponseEntity<List<DetalleTramo>> objTramos = rest.exchange(urlBackend + "/detalleTramo/lista", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DetalleTramo>>() {
				});
		List<DetalleTramo> lstTramosMarca = objTramos.getBody();
		return lstTramosMarca;
	}
	
	private Vector<Object> armaDetalles(List<DetalleTramo> lstDetalles) {
		SimpleDateFormat formato = new SimpleDateFormat("HH:mm");
		setHshDetalles(new LinkedHashMap<>());
		setLstDetalleTramo(new Vector<>());
		
		Vector<Object> vDatos = new Vector<>();
		Iterator<?> it = lstDetalles.iterator();
		while(it.hasNext()){ 
			Vector<Object> renglon = new Vector<>();
			DetalleTramo detalle = (DetalleTramo)it.next();
			renglon.add(detalle);
			renglon.add(detalle.getKey().getTramoId());
			renglon.add(detalle.getKey().getDiaSemana());
			renglon.add(formato.format(detalle.getKey().getInicio()));
			renglon.add(formato.format(detalle.getFin()));
			renglon.add(formato.format(detalle.gettRecorrido()));
			vDatos.add(renglon);
			
			String llave =  detalle.getKey().getTramoId() + "-"+ detalle.getKey().getDiaSemana();
			if(getHshDetalles().containsKey(llave)){ 
				Vector<Object> datos = (Vector<Object>) getHshDetalles().get(llave);
				datos.add(detalle);
			}else{ 
				Vector<Object> datos = new Vector<>();
				datos.add(detalle);
				getHshDetalles().put(llave, datos);
			}
			getLstDetalleTramo().add(detalle);
		}
		return vDatos;
	}

	private void armaSecuenciaViajes(DetalleRol detalles, Date fecInicial, Date fechaFinal, int numDias) {

		Marca marca = recuperaMarca(getDescMarca());
		MarcaRol marcaRol = recuperaMarcaRol(getDescMarcaRol());
		Rol rol = new Rol();
		rol.setRolId(1);
		for (int i = 1; i <= numDias; i++) {
			Viajes viaje = new Viajes();
			viaje.setRol(rol);
			viaje.setCuadro(detalles.getCuadro());
			viaje.setSecuencia(detalles.getSecuencia());
			viaje.setOrigen(detalles.getOrigen());
			viaje.setDestino(detalles.getDestino());
			viaje.setKms(detalles.getKms());
			viaje.setEstatusViaje(new Integer(1));
			viaje.setFecHorAct(new Date());
			viaje.setUsuarioId(1);
			viaje.setMarca(marca);
			viaje.setMarcarol(marcaRol);
			
//			TramoMarca tramosMarca = (TramoMarca) getHshTramosMarca().get(detalles.getOrigen().getCveParada() + "-" + detalles.getDestino().getCveParada() + "-" + detalles.getViaId());
			
			if (i != 1) {
				fecInicial = cambiafechaDia(fecInicial);
			}
			int diaSemana = getDayOfWeek(fecInicial);

			if (Constantes.DIA_DOMINGO == diaSemana) {
				if (detalles.getDomingo() != null && detalles.getDomingo().intValue() == 1) {
					aplicaCambiosviaje(viaje, fecInicial, detalles.getHoraSalida(), detalles.getValDomingo(), "D", null);
				} else {
					cambiafechaDia(fecInicial);
					continue;
				}
			} else if (Constantes.DIA_LUNES == diaSemana) {
				if (detalles.getLunes() != null && detalles.getLunes().intValue() == 1) {
					aplicaCambiosviaje(viaje, fecInicial, detalles.getHoraSalida(), detalles.getValLunes(), "L", null);
				} else {
					cambiafechaDia(fecInicial);
					continue;
				}
			} else if (Constantes.DIA_MARTES == diaSemana) {
				if (detalles.getMartes() != null && detalles.getMartes().intValue() == 1) {
					aplicaCambiosviaje(viaje, fecInicial, detalles.getHoraSalida(), detalles.getValMartes(), "M", null);
				} else {
					cambiafechaDia(fecInicial);
					continue;
				}
			} else if (Constantes.DIA_MIERCOLES == diaSemana) {
				if (detalles.getMiercoles() != null && detalles.getMiercoles().intValue() == 1) {
					aplicaCambiosviaje(viaje, fecInicial, detalles.getHoraSalida(), detalles.getValMiercoles(), "W", null);
				} else {
					cambiafechaDia(fecInicial);
					continue;
				}
			} else if (Constantes.DIA_JUEVES == diaSemana) {
				if (detalles.getJueves() != null && detalles.getJueves().intValue() == 1) {
					aplicaCambiosviaje(viaje, fecInicial, detalles.getHoraSalida(), detalles.getValJueves(), "J", null);

				} else {
					cambiafechaDia(fecInicial);
					continue;
				}
			} else if (Constantes.DIA_VIERNES == diaSemana) {
				if (detalles.getViernes() != null && detalles.getViernes().intValue() == 1) {
					aplicaCambiosviaje(viaje, fecInicial, detalles.getHoraSalida(), detalles.getValViernes(), "V", null);

				} else {
					cambiafechaDia(fecInicial);
					continue;
				}
			} else if (Constantes.DIA_SABADO == diaSemana) {
				if (detalles.getSabado() != null && detalles.getSabado().intValue() == 1) {
					aplicaCambiosviaje(viaje, fecInicial, detalles.getHoraSalida(), detalles.getValSabado(), "S", null);
				} else {
					cambiafechaDia(fecInicial);
					continue;
				}
			}
			getLstViaje().add(viaje);
			guardaViaje(viaje);
		}
		System.out.println();
	}
	
	private void guardaViaje(Viajes viaje) { 
		rest = new RestTemplate();
		
		rest.postForEntity(urlBackend + "/viajes/save", viaje, Viajes.class);
		
		System.out.println("");
	}
	
	private Viajes aplicaCambiosviaje(Viajes viaje, Date fechaAplica, Time horasSalida, Double valorDia, String diaAplica, TramoMarca tramosMarca) {
		viaje.setFecha(fechaAplica);
		viaje.setHoraSalida(horasSalida == null ? null : sumaHrsMin(fechaAplica, horasSalida));
		viaje.setCostoViaje(valorDia == null ? null : valorDia);
		viaje.setDiaAplicacion(diaAplica);

		Date fechaHoraSalida = sumaHrsMin(fechaAplica, horasSalida);
		
		String llave =  11+"-"+ diaAplica;
		Date tRecorrido = verificaHorarioTiempoRecorrido(fechaHoraSalida, llave);
		
		viaje.setFechaLlegada(tRecorrido == null ? null : sumaHrsMin(fechaHoraSalida, tRecorrido));
		return viaje;
	}

	private Date verificaHorarioTiempoRecorrido(Date fechaHoraSalida, String llave) {
		
		Vector<Object> lstDetalleTramo = (Vector<Object>)getHshDetalles().get(llave);
		
		Iterator<?> it = lstDetalleTramo.iterator();
		while(it.hasNext()){
			DetalleTramo detalle = (DetalleTramo) it.next();
			
			Time viaje = new Time(formatoTiempo(fechaHoraSalida).getTime());
			Time inicio = new Time(detalle.getKey().getInicio().getTime());
			Time fin = new Time(detalle.getFin().getTime());
			
			if((viaje.equals(inicio) || viaje.after(inicio)) && (viaje.equals(fin) || viaje.before(fin))){
				return detalle.gettRecorrido();
			}
		}
		return null;
	}

	private void cargaTramosMarca(Marca marca) {
		rest = new RestTemplate();
		ResponseEntity<List<TramoMarca>> objMarca = rest.exchange(urlBackend + "/tramoMarca/porMarcaId/{id}", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TramoMarca>>() { }, marca.getId());
		List<TramoMarca> lstTramosMarca = objMarca.getBody();
		armaDatos(lstTramosMarca);
		System.out.println();
	}
	
	private void armaDatos(List<TramoMarca> lstRolMarcaTramo) 
	{
		setHshTramosMarca(new LinkedHashMap<>());
		try {
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			if( lstRolMarcaTramo == null || lstRolMarcaTramo.isEmpty() ) return;
			
			Iterator<?> it = lstRolMarcaTramo.iterator();
			while(it.hasNext()){
				Vector<Object> renglon = new Vector<>();
				TramoMarca rol = (TramoMarca) it.next();
				
				String origen = rol.getOrigen().getCveParada();
				String destino = rol.getDestino().getCveParada();
				
				renglon.add(rol);
				renglon.add(rol.getMarca().getDescMarca());
				renglon.add(origen);
				renglon.add(destino);
				renglon.add(rol.getKmsRecorridos());
				renglon.add(rol.getCosto());
				renglon.add(rol.getVia().getNombVia());
				String llave = rol.getMarca().getId()+"-"+rol.getOrigen().getParadaId()+"-"+rol.getDestino().getParadaId()+"-"+rol.getVia().getId();

				
				getHshTramosMarca().put(origen+"-"+destino+"-"+rol.getVia().getId(), rol);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private Date formatoTiempo(Date fechaHoraSalida){
		Calendar cal = Calendar.getInstance();
		cal.setTime(fechaHoraSalida);
		cal.set(Calendar.YEAR, 1970);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, 01);
		return cal.getTime();
	}
	private Date cambiafechaDia(Date fecInicial) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecInicial);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}
	
	private void pintaviajesconsola() {
		SimpleDateFormat formaro = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		SimpleDateFormat horas = new SimpleDateFormat("HH:mm:ss");
		Iterator<?> it = getLstViaje().iterator();
		while (it.hasNext()) {
			Viajes viaje = (Viajes) it.next();
			String descripcion = "  Cuadro : " + viaje.getCuadro() + " Secuencia : " + viaje.getSecuencia()
					+ "  Fecha : " + formaro.format(viaje.getFecha()) + " Horas : ";
			if (viaje.getHoraSalida() != null) {
				descripcion += " " + horas.format(viaje.getHoraSalida());
			}
			System.out.println(descripcion);
		}

	}
	
	public boolean creaDetalleRol() {
		setDetallesRol(new Vector<>());
		Vector<Object> detalle = getOfertaViajes();
		Iterator<?> it = detalle.iterator();
		while (it.hasNext()) {
			ValorOferta valor = (ValorOferta) it.next();

			DetalleRol detalleRol = new DetalleRol();
//			Parada origen = (Parada) getHshParadas().get(valor.getOrigen());
//			Parada destino = (Parada) getHshParadas().get(valor.getDestino());
//			Via via = (Via) getHshVias().get(valor.getVia());
//			if (origen == null) {
//				setMensaje("No se pudo guardar el archivo porque el origen no existe");
//				return false;
//			} else if (destino == null) {
//				setMensaje("No se pudo guardar el archivo porque el destino no existe");
//				return false;
//			}

			detalleRol.setRolId(1);// realizar busqueda
			detalleRol.setCuadro(new Integer(valor.getCuadro()));
			detalleRol.setSecuencia(valor.getSecuencia());
			detalleRol.setHoraSalida(valor.getHoraSalida() == null ? null : new Time(valor.getHoraSalida().getTime()));

//			TramoMarca tramosMarca = (TramoMarca) getHshTramosMarca()
//					.get(origen.getCveParada() + "-" + destino.getCveParada() + "-" + via.getId());

			detalleRol.setViaId(null);//tramosMarca == null ? null : tramosMarca.getKey().getVia().getId());// catalogo
			detalleRol.setKms(null);//tramosMarca == null ? null : tramosMarca.getKmsRecorridos());
			detalleRol.setOrigen(null);//origen);
			detalleRol.setDestino(null);//destino);

			detalleRol.setLunes(valor.getAplicaL().equals("") ? null : new Integer(valor.getAplicaL()));
			detalleRol.setMartes(valor.getAplicaM().equals("") ? null : new Integer(valor.getAplicaM()));
			detalleRol.setMiercoles(valor.getAplicaMI().equals("") ? null : new Integer(valor.getAplicaMI()));
			detalleRol.setJueves(valor.getAplicaJ().equals("") ? null : new Integer(valor.getAplicaJ()));
			detalleRol.setViernes(valor.getAplicaV().equals("") ? null : new Integer(valor.getAplicaV()));
			detalleRol.setSabado(valor.getAplicaS().equals("") ? null : new Integer(valor.getAplicaS()));
			detalleRol.setDomingo(valor.getAplicaD().equals("") ? null : new Integer(valor.getAplicaD()));
			detalleRol.setValLunes(valor.getValorL().equals("") ? null : new Double(valor.getValorL()));
			detalleRol.setValMartes(valor.getValorM().equals("") ? null : new Double(valor.getValorM()));
			detalleRol.setValMiercoles(valor.getValorMI().equals("") ? null : new Double(valor.getValorMI()));
			detalleRol.setValJueves(valor.getValorJ().equals("") ? null : new Double(valor.getValorJ()));
			detalleRol.setValViernes(valor.getValorV().equals("") ? null : new Double(valor.getValorV()));
			detalleRol.setValSabado(valor.getValorS().equals("") ? null : new Double(valor.getValorS()));
			detalleRol.setValDomingo(valor.getValorD().equals("") ? null : new Double(valor.getValorD()));

			detalleRol.setFecHorAct(new Date());
			detalleRol.setUsuarioId(1);
			getDetallesRol().add(detalleRol);
		}
//		guardaDetalle(getDetallesRol());
		return true;
	}

	public Vector<Object> getDetallesRol() {
		return detallesRol;
	}

	public void setDetallesRol(Vector<Object> detallesRol) {
		this.detallesRol = detallesRol;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Vector<Object> getOfertaViajes() {
		return ofertaViajes;
	}

	public void setOfertaViajes(Vector<Object> ofertaViajes) {
		this.ofertaViajes = ofertaViajes;
	}

	public Vector<Object> getLstViaje() {
		return lstViaje;
	}

	public void setLstViaje(Vector<Object> lstViaje) {
		this.lstViaje = lstViaje;
	}

	public String getDescMarca() {
		return descMarca;
	}

	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}

	public String getDescMarcaRol() {
		return descMarcaRol;
	}

	public void setDescMarcaRol(String descMarcaRol) {
		this.descMarcaRol = descMarcaRol;
	}

	public int obtieneDiferenciaDias(Date pOFecha1, Date pOFecha2) {
        int myDias = (int) ((pOFecha1.getTime() - pOFecha2.getTime()) / 86400000L);
        return myDias;
    }
	
	public int getDayOfWeek(Date aFecha) {
        GregorianCalendar myCalendar = new GregorianCalendar();
        myCalendar.setTimeInMillis(aFecha.getTime());
        myCalendar.setGregorianChange(aFecha);
        return myCalendar.get(Calendar.DAY_OF_WEEK);
    }
	
	public Date sumaHrsMin(Date dFecha, Date dSumar) {
        Date miSuma = null;
        Calendar oC = Calendar.getInstance();
        oC.clear();
        oC.setTimeInMillis(dFecha.getTime());
        Calendar cSumar = Calendar.getInstance();
        cSumar.clear();
        cSumar.setTimeInMillis(dSumar.getTime());
        int horas = cSumar.get(Calendar.HOUR_OF_DAY);
        int mins = cSumar.get(Calendar.MINUTE);
        oC.add(Calendar.HOUR, horas);
        oC.add(Calendar.MINUTE, mins);
        miSuma = oC.getTime();
        return miSuma;
    }

	public Vector<Object> getLstDetalleTramo() {
		return lstDetalleTramo;
	}

	public void setLstDetalleTramo(Vector<Object> lstDetalleTramo) {
		this.lstDetalleTramo = lstDetalleTramo;
	}

	public LinkedHashMap<Object, Object> getHshDetalles() {
		return hshDetalles;
	}

	public void setHshDetalles(LinkedHashMap<Object, Object> hshDetalles) {
		this.hshDetalles = hshDetalles;
	}

	public LinkedHashMap<Object, Object> getHshTramosMarca() {
		return hshTramosMarca;
	}

	public void setHshTramosMarca(LinkedHashMap<Object, Object> hshTramosMarca) {
		this.hshTramosMarca = hshTramosMarca;
	}
}
