package cuentas.front.controller.xls;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import cuentas.back.ccuentas.modelo.GeneracionCuadros;

public class GeneraXlsCuadros { 

	public ByteArrayInputStream generaExcelCuadros(GeneracionCuadros cuadros) {
		LinkedHashMap<String, Object[]> datos = new LinkedHashMap<String, Object[]>();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		
		Workbook workbook = new HSSFWorkbook();
		// Crea hoja nueva
		Sheet sheet = workbook.createSheet("Por Cuadros");
		armaEncabezado(datos, cuadros);
		datos.put("6", new Object[] { "Cuadro", "Secuencia", "Hora Salida", "Origen", "Destino", "kms", "Via", " L ", " M ",
						" M ", " J ", " V ", " S ", " D ", "Num Dias", " L ", " M ", " M ", " J ", " V ", " S ",
						" D " });
		armaRenglones(datos, cuadros);
		
//		 Iterar sobre datos para escribir en la hoja
		Set<String> keyset = datos.keySet();
		
		int numeroRenglon = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(numeroRenglon++);
			Object[] arregloObjetos = datos.get(key);
			int numeroCelda = 0;
			for (Object obj : arregloObjetos) {
				Cell cell = row.createCell(numeroCelda++);
				if (obj instanceof String) {
					cell.setCellValue((String) obj);
				} else if (obj instanceof Integer) {
					cell.setCellValue((Integer) obj);
				}
			}
		}
		
		try {
			workbook.write(stream);
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ByteArrayInputStream(stream.toByteArray());
	}

	private Map<String, Object[]> armaEncabezado(Map<String, Object[]> datos, GeneracionCuadros cuadros) {
		datos.put("1", new Object[] { "Marca", cuadros.getMarcaRol() });
		datos.put("3", new Object[] { "Num Cuadros", cuadros.getCuadros().toString() });
		datos.put("4", new Object[] { "Viajes Maximos por cuadro", cuadros.getVueltas().toString() });
		datos.put("5", new Object[] { "Tipo Oferta", cuadros.getTipoOferta() });
		return datos;
	}

	private void armaRenglones(Map<String, Object[]> datos, GeneracionCuadros cuadros) {
		int consecutivo = 7;
		Vector<Object> abc = getAbc();
		for (int a = 1; a <= cuadros.getCuadros().intValue(); a++) {
			for (int i = 1; i <= cuadros.getVueltas().intValue(); i++) {
				datos.put(consecutivo + "", new Object[] { a, "" + a + abc.get(i) });
				consecutivo++;
			}
		}
	}

	public Vector<Object> getAbc() {
		Vector<Object> lstABC = new Vector<>();

		lstABC.add("");
		lstABC.add("A");
		lstABC.add("B");
		lstABC.add("C");
		lstABC.add("D");
		lstABC.add("E");
		lstABC.add("F");
		lstABC.add("G");
		lstABC.add("H");
		lstABC.add("I");
		lstABC.add("J");
		lstABC.add("K");
		lstABC.add("L");
		lstABC.add("M");
		lstABC.add("N");
		lstABC.add("O");
		lstABC.add("P");
		lstABC.add("Q");
		lstABC.add("R");
		lstABC.add("S");
		lstABC.add("T");
		lstABC.add("U");
		lstABC.add("V");
		lstABC.add("W");
		lstABC.add("X");
		lstABC.add("Y");
		lstABC.add("Z");
		return lstABC;
	}
}
